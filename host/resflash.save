#!/bin/sh

# Save files on shutdown as directed in resflash.conf
# Copyright Brian Conway <bconway@rcesoftware.com>, see LICENSE for details

set -o errexit
set -o nounset
if set -o|fgrep -q pipefail; then
  set -o pipefail
fi

. /etc/resflash.conf

if [ -n "${save_etc+1}" ] || [ -n "${save_var+1}" ] || \
   [ -n "${relink_on_shutdown+1}" ]; then
  mount -s /cfg
  trap 'sync; umount /cfg; exit 1' ERR INT

  mkdir -p /cfg/{etc,var,tmp} /cfg/upgrade_overlay/{etc,home,root,var}
  chmod 700 /cfg/upgrade_overlay/root
  cp -p /etc/resflash.conf /cfg/etc
  # tar -C doesn't play nicely with glob(3)
  cwd=$(pwd)

  if [ -n "${save_etc+1}" ]; then
    echo 'resflash: Saving /etc files'
    cd /etc
    for saver in ${save_etc}; do
      if [ ${saver} == 'fstab' ]; then
        echo '*** WARNING: /etc/fstab conflicts with upgrades, skipping. ***'
      else
        if [ -e /cfg/etc/${saver} ]; then
          rand=/cfg/tmp/${RANDOM}
          mkdir -p ${rand}
          mv /cfg/etc/${saver} ${rand}
        fi
        tar cf - ${saver}|tar xpf - -C /cfg/etc
      fi
    done
  fi

  if [ -n "${save_var+1}" ]; then
    echo 'resflash: Saving /var files'
    cd /var
    for saver in ${save_var}; do
      if [ -e /cfg/var/${saver} ]; then
        rand=/cfg/tmp/${RANDOM}
        mkdir -p ${rand}
        mv /cfg/var/${saver} ${rand}
      fi
      tar cf - ${saver}|tar xpf - -C /cfg/var
    done
  fi

  # Re-populating /etc/fstab conflicts with upgrades and is always a mistake
  rm -f /cfg/etc/fstab

  cd ${cwd}
  rm -rf /cfg/tmp
  sync
  umount /cfg
  sync
fi
