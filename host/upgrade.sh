#!/bin/sh

# Upgrade the inactive root partition with a new filesystem and activate it
# Copyright Brian Conway <bconway@rcesoftware.com>, see LICENSE for details

set -o errexit
set -o nounset
if set -o|fgrep -q pipefail; then
  set -o pipefail
fi
#set -o xtrace # DEBUG

BINDIR=$(dirname ${0})
. ${BINDIR}/resflash.sub

MACHINE=$(machine)
set_attr_by_machine ${MACHINE}

# Parse out disks and partitions

DUID=$(fgrep ' / ' /etc/fstab|cut -d ' ' -f 1|cut -d . -f 1)
CURRPART=$(fgrep ' / ' /etc/fstab|cut -d ' ' -f 1|cut -d . -f 2)
if [ ${CURRPART} == 'd' ]; then
  NEWPART=e
else
  NEWPART=d
fi
rootpart=$(mount|fgrep ' on / '|cut -d ' ' -f 1|cut -d / -f 3)
DISK=${rootpart%?}

# Leave one set of logs for debugging
rm -rf /tmp/resflash.??????
MNTPATH=$(mktemp -t -d resflash.XXXXXX)

# Write filesystem to the inactive partition

echo 'Writing filesystem to inactive partition'
(tee /dev/fd/3|dd of=/dev/r${DISK}${NEWPART} ibs=8k obs=1m >> \
${MNTPATH}/00.upgrade.dd.fs 2>&1;) 3>&1|cksum -a ${ALG}

# Verify the newly written partition

echo 'Checking filesystem'
if ! fsck -fp ${DUID}.${NEWPART}; then
  echo 'Filesystem failure, fsck failed. Inspecting filesystem:'
  dd if=/dev/r${DISK}${NEWPART} bs=96k count=1 status=none|file -
  exit 1
fi

# Update fstab for the current duid and new partition

mkdir -p ${MNTPATH}/fs
mount -o noatime ${DUID}.${NEWPART} ${MNTPATH}/fs
mount -s /cfg
mount -s /mbr
trap "sync; umount ${MNTPATH}/fs; umount /cfg; umount /mbr; exit 1" ERR INT

if [ -d /cfg/upgrade_overlay ]; then
  echo 'Overlaying data from /cfg/upgrade_overlay to filesystem'
  rm -f /cfg/upgrade_overlay/etc/fstab
  tar cf - -C /cfg/upgrade_overlay .|tar xpf - -C ${MNTPATH}/fs
fi
umount /cfg

echo 'Updating fstab'
fsduid=$(fgrep ' / ' ${MNTPATH}/fs/etc/fstab|cut -d ' ' -f 1|cut -d . -f 1)
sed -i -e "s/${fsduid}/${DUID}/" \
    -e "/^${DUID}.d/s/${DUID}.d/${DUID}.${NEWPART}/" \
    ${MNTPATH}/fs/etc/fstab

# Update MBR, biosboot(8), boot(8) on amd64/i386, kernels on octeon/macppc, and
# boot.conf most places

if [ -f ${MNTPATH}/fs/usr/mdec/mbr ]; then
  echo 'Updating MBR'
  fdisk -uy -f ${MNTPATH}/fs/usr/mdec/mbr ${DUID} >> \
  ${MNTPATH}/01.upgrade.fdisk.updatembr 2>&1
fi

if [ ${MACHINE} == 'amd64' ] || [ ${MACHINE} == 'i386' ]; then
  # Binaries for installboot, biosboot(8), and boot(8) must match
  echo 'Updating biosboot(8) and boot(8)'
  installboot -v -r /mbr ${DUID} /usr/mdec/biosboot /usr/mdec/boot >> \
  ${MNTPATH}/02.upgrade.installboot.pc 2>&1
elif [ ${MACHINE} == 'octeon' ]; then
  mount -s /${DOSMNT}
  echo "Updating /${DOSMNT} kernels"
  rm -f /${DOSMNT}/bsd.{d,e}
  mv /${DOSMNT}/bsd /${DOSMNT}/bsd.${CURRPART}
  cp ${MNTPATH}/fs/bsd.${NEWPART} /${DOSMNT}/bsd
  rm -f ${MNTPATH}/fs/bsd
  ln ${MNTPATH}/fs/bsd.${NEWPART} ${MNTPATH}/fs/bsd
  if [ -f ${MNTPATH}/fs/bsd.rd ]; then
    cp ${MNTPATH}/fs/bsd.rd /${DOSMNT}
  fi
  sync
  umount /${DOSMNT}
elif [ ${MACHINE} == 'macppc' ]; then
  echo 'Updating /mbr kernels and regenerating random.seed'
  cp -p ${MNTPATH}/fs/bsd.${NEWPART} /mbr
  rm -f ${MNTPATH}/fs/bsd
  ln ${MNTPATH}/fs/bsd.${NEWPART} ${MNTPATH}/fs/bsd
  if [ -f ${MNTPATH}/fs/bsd.rd ]; then
    cp -p ${MNTPATH}/fs/bsd.rd /mbr
  fi
  dd if=/dev/random of=/mbr/etc/random.seed bs=512 count=1 status=none
fi

# Update ${DOSBOOTBINS} bootloaders, if applicable

if disklabel ${DUID}|grep -q 'i:.*MSDOS' && fgrep -q /${DOSMNT} /etc/fstab \
&& [ -n "${DOSBOOTBINS}" ]; then
  mount -s /${DOSMNT}
  echo "Updating ${DOSBOOTBINS} bootloader(s)"
  for bootbin in ${DOSBOOTBINS}; do
    if [ -f ${MNTPATH}/fs/usr/mdec/${bootbin} ]; then
      cp ${MNTPATH}/fs/usr/mdec/${bootbin} /${DOSMNT}/${DOSBOOTDIR}
    fi
  done
  sync
  umount /${DOSMNT}
fi

sync
umount ${MNTPATH}/fs

# Set the new partition active

echo 'Everything looks good, setting the new partition active'
if [ ${MACHINE} == 'amd64' ] || [ ${MACHINE} == 'i386' ]; then
  sed -i "/^set device hd0/s/hd0[a-p]/hd0${NEWPART}/" /mbr/etc/boot.conf
elif [ ${MACHINE} == 'macppc' ]; then
  sed -i "/^set image/s/bsd.[de]/bsd.${NEWPART}/" /mbr/etc/boot.conf
fi

sync
umount /mbr

echo 'Upgrade complete!'
