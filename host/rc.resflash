#!/bin/sh

# Early rc script to set up mfs filesystems
# Copyright Brian Conway <bconway@rcesoftware.com>, see LICENSE for details

set -o errexit
set -o nounset
if set -o|fgrep -q pipefail; then
  set -o pipefail
fi

# Directories to relocate and their mfs sizes (MB or auto)
# Constrained devices (< 256 MB) should not use reorder/relink mfs mounts.
if [ $(sysctl -n hw.physmem) -gt 262144000 ]; then
  RELOCDIRS='dev.auto etc.auto var.64 usr/lib.auto usr/libexec.auto usr/share/relink.80'
else
  RELOCDIRS='dev.auto etc.auto var.64'
fi

# Args: 1 - dir
# Assigns: mfssizemb
calc_auto_size() {
  dirsizekb=$(du -ks ${1}|awk -safe '{ print $1 }')

  # Directories greater than 16 MB get 25% headroom, less get 50%
  if [ ${dirsizekb} -gt 16384 ]; then
    mfssizemb=$((dirsizekb * 4 / 3 / 1024))
  else
    mfssizemb=$((dirsizekb * 2 / 1024))
  fi

  # Set a floor of 6 MB
  if [ ${mfssizemb} -lt 6 ]; then
    mfssizemb=6
  fi
}

# Undo /etc/rc's 'mount -uw /'
mount -ur /
sync

# Check for conflicting mounts from fstab
if mount|egrep -q 'on /dev/|on /etc/|on /var/|on /usr/lib/|on /usr/libexec/|on /usr/share/relink/'; then
  echo -n 'resflash: *** WARNING: Mounts conflict with populating mfs:'
  for mnt in $(mount|egrep 'on /dev/|on /etc/|on /var/|on /usr/lib/|on /usr/libexec/|on /usr/share/relink/'\
  |cut -d ' ' -f 3); do
    echo -n " ${mnt}"
    umount ${mnt}
  done
  echo ', unmounting. Use noauto. ***'
fi

RELOCPATH=$(mktemp -t -d resflash.XXXXXX)

echo -n 'resflash: Relocating'
for relocdir in ${RELOCDIRS}; do
  dir=$(echo ${relocdir}|cut -d . -f 1)
  tarname=$(echo ${dir}|tr -d /)
  dirsuff=$(echo ${relocdir}|cut -d . -f 2)

  if [ -d /${dir} ]; then
    if [ ${dirsuff} == 'auto' ]; then
      calc_auto_size ${dir}
    else
      mfssizemb=${dirsuff}
    fi

    echo -n " /${dir}"
    mount -t mfs -o noatime,nodev,noexec,-s${mfssizemb}M swap \
    ${RELOCPATH}

    # Don't include kernel relinking in mfs relocating
    case ${dir} in
      usr/share/relink) tar cf ${RELOCPATH}/${tarname}.tar -C /${dir} usr;;
      *) tar cf ${RELOCPATH}/${tarname}.tar -C /${dir} .;;
    esac

    case ${dir} in
      dev) mount -t mfs -o noatime,noexec,-b4096,-f512,-s${mfssizemb}M \
           swap /${dir};;
      etc|usr/lib|usr/share/relink) mount -t mfs -o \
           noatime,nodev,nosuid,-s${mfssizemb}M swap /${dir};;
      var) mount -t mfs -o noatime,nodev,noexec,-s${mfssizemb}M \
           swap /${dir};;
      usr/libexec) mount -t mfs -o noatime,nodev,-s${mfssizemb}M swap \
           /${dir};;
    esac

    tar xpf ${RELOCPATH}/${tarname}.tar -C /${dir}
    rm ${RELOCPATH}/${tarname}.tar
    umount ${RELOCPATH}
  fi
done
echo ' to mfs'

rm -r ${RELOCPATH}

# Safe to add nodev to / now /dev is mfs, update fstab to survive 'mount -a'
mount -u -o nodev /
sed -i '/^.*\.[de] \/ /s/noatime /noatime,nodev /' /etc/fstab

echo 'resflash: Overlaying data from /cfg'
mount -o ro /cfg
tar cf - -C /cfg/etc .|tar xpf - -C /etc
tar cf - -C /cfg/var .|tar xpf - -C /var
umount /cfg
